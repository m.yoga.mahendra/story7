(function(){
    $(".toggle-dark").click(toDark);
    $(".accordion").accordion({
        collapsible: true,
        active: false
    });
})();

function toDark() {
    $("html").css("background-color", "rgba(0, 0, 0,1)");
    $("body").css("background-color", "rgba(0, 0, 0,1)");
    $(".texts").css("color", "white");
    $(".ui-accordion-header").css("background-color", "rgb(160, 160, 160)");
    $(".ui-accordion-header").css("color", "white");
    $(".ui-accordion-header-active").css("background-color", "rgb(200,200,200)");
    $(".ui-accordion-content").css("background-color", "rgb(80, 80, 80)");
    $(".ui-accordion-content").css("color", "white");
    $(".ui-accordion-content").css("border", "none");
    $(".toggle-dark").click(toLight);
    document.getElementById("status").innerHTML = "Dark Mode";
}

function toLight() {
    $("html").css("background-color", "rgba(255, 255, 255,1)");
    $("body").css("background-color", "rgba(255, 255, 255,1)");
    $(".ui-accordion-content").css("background-color", "rgb(160, 160, 160)");
    $(".ui-accordion-content").css("border", "1px solid #dddddd");
    $(".ui-accordion-content").css("color","black");    
    $(".ui-accordion-header").css("color", "black");
    $(".ui-accordion-header").css("background-color", "rgb(80,80,80)");
    $(".ui-accordion-header-active").css("background-color", "rgb(50,50,50)");
    $(".texts").css("color", "black");
    $(".toggle-dark").click(toDark);
    document.getElementById("status").innerHTML = "Light Mode";
}