import time

from django.test import Client, LiveServerTestCase, TestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

from .views import index


# Django Test
class IndexTest(TestCase):
    def test_index_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_view(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

# Selenium Test
class IndexLiveTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(IndexLiveTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(IndexLiveTest, self).tearDown()

    def test_check_title(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(3)
        self.assertEqual(selenium.title, 'Story 7 Yoga')
        
    def test_default_theme(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(1)
        html = selenium.find_element_by_css_selector('html')
        background_color = html.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(255, 255, 255, 1)')

    def test_dark_theme(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(1)
        change_theme = selenium.find_element_by_css_selector('.toggle-dark')
        change_theme.click()
        time.sleep(5)
        html = selenium.find_element_by_css_selector('html')
        background_color = html.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(0, 0, 0, 1)')
        
